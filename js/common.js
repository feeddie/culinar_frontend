$(document).ready(function() {
	
		(function() {
			/* ========= Tabs ========= */
			$(".command__tabs-link").on('click', function(e) {
				e.preventDefault();

				var 
					that = $(this),
					item = that.closest('.command__tabs-item'),
					itemInd = item.index(),
					content = $('.command__content-item');

				item
					.addClass('command__tabs-item_active')
					.siblings()
					.removeClass('command__tabs-item_active');

				content.eq(itemInd)
					.addClass('command__content-item_active')
					.siblings()
					.removeClass('command__content-item_active');
			});
		})();

		(function() {
			/* ========= Akkardeon ========= */
			$(".question__drop-link").on('click', function(e) {
				e.preventDefault();

				var
					that = $(this),
					item = that.closest('.question__drop-item'),
					content = item.find('.question__drop-item-desc');

				if(!item.hasClass('question__drop-item_active')){
					item
							.find('.question__drop-item-desc')
							.slideDown(function() {
									item
										.addClass('question__drop-item_active')
							});
						item
									.siblings()
									.find('.question__drop-item-desc')
									.slideUp(function() {
										item
											.siblings()
											.removeClass('question__drop-item_active')
									})	
				}

			});
		})();

		(function() {
			/* ========= Slider ========= */
    	$('.slider__list').bxSlider();

		})();

});